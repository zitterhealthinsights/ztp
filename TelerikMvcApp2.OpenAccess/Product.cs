﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TelerikMvcApp2.OpenAccess
{
    public class Product
    {
        public int ID { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
    }
}
